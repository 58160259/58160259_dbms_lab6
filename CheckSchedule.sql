drop procedure if exists CheckSchedule;

DELIMITER $$

CREATE procedure CheckSchedule (in SchID varchar(10), in MvID varchar(10),
				out CinID varchar(10), out TimeSch DateTime)
Begin
	

	select CinemaID , TimeSchedule

	into CinID , TimeSch
	From MovieSchedules
	where
		ScheduleID = SchID and MovieID = MvID;
	

End $$
DELIMITER ;
