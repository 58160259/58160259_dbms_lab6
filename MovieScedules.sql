Create Table MovieSchedules
(
	ScheduleID	varchar(10)	not null,
	ScheduleName	varchar(50)	not null,
	MovieID		varchar(10)	not null,
	CinemaID	varchar(10)	not null,
	TimeSchedule	DateTime	not null,
	PRIMARY KEY(ScheduleID),
	FOREIGN KEY(MovieID) REFERENCES	Movies(MovieID),
	FOREIGN KEY(CinemaID) REFERENCES Cinema(CinemaID)
) Engine=InnoDB;
