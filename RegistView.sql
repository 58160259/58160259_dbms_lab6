Create View Regist AS
	
	Select R.RegistID , MS.ScheduleID , MS.ScheduleName , C.CustomerName , C.CustomerSurname

	FROM Register AS R
	LEFT join (MovieSchedules AS MS, Customers AS C )
	ON R.ScheduleID = MS.ScheduleID and 
	   R.CustomerID = C.CustomerID;
		
